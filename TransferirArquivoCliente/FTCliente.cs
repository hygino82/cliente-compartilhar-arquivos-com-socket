﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TransferirArquivoCliente
{
    class FTCliente
    {
        static IPEndPoint ipEnd_Cliente;
        static Socket clienteSocket_cliente;
        public static string EnderecoIP = "127.0.0.1";
        public static int PortaHost = 1000;
        public static string PastaRecepcaoArquivos = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\";
        public static Label LabelMenssagem;

        public static void EnviarArquivo(string arquivo)
        {
            try
            {
                ipEnd_Cliente = new IPEndPoint(IPAddress.Parse(EnderecoIP), PortaHost);
                clienteSocket_cliente = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

                string pasta = "";

                pasta = arquivo.Substring(0, arquivo.LastIndexOf(@"\") + 1);
                arquivo = arquivo.Substring(arquivo.LastIndexOf(@"\") + 1);

                byte[] nomeArquivoByte = Encoding.UTF8.GetBytes(arquivo);

                if (nomeArquivoByte.Length > 50000 * 1024)
                {
                    LabelMenssagem.Invoke(new Action(() =>
                    {
                        LabelMenssagem.ForeColor = Color.Red;
                        LabelMenssagem.Text = "O tamanho do arquivo é maior que 50Mb, tente um arquivo menor.";
                    }));
                    return;
                }

                string caminhoCompleto = pasta + arquivo;

                byte[] fileData = File.ReadAllBytes(caminhoCompleto);
                byte[] clientData = new byte[4 + nomeArquivoByte.Length + fileData.Length];
                byte[] nomeArquivoLen = BitConverter.GetBytes(nomeArquivoByte.Length);

                nomeArquivoLen.CopyTo(clientData, 0);
                nomeArquivoByte.CopyTo(clientData, 0);
                fileData.CopyTo(clientData, 4 + nomeArquivoByte.Length);
                clienteSocket_cliente.Connect(ipEnd_Cliente);
                clienteSocket_cliente.Send(clientData, 0, clientData.Length, 0);
                clienteSocket_cliente.Close();

                LabelMenssagem.Invoke(new Action(() =>
                {
                    LabelMenssagem.ForeColor = Color.Blue;
                    LabelMenssagem.Text = "Arquivo [" + arquivo + "] tranferido";
                }));
            }
            catch (Exception ex)
            {
                LabelMenssagem.Invoke(new Action(() =>
                {
                    LabelMenssagem.ForeColor = Color.Red;
                    LabelMenssagem.Text = "Falha o servidor não está atendendo: " + ex.Message;
                }));
            }
            finally
            {
                clienteSocket_cliente.Disconnect(false);
                clienteSocket_cliente.Close();

            }
        }
    }
}
